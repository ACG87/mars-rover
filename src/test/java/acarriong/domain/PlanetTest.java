package acarriong.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;

import org.assertj.core.util.Arrays;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import acarriong.domain.enums.MarsRoverEnums.Terrain;
import org.junit.Assert;

@RunWith(Parameterized.class)
public class PlanetTest {

	@Parameters
	public static List<Object> data() {
		return Arrays.asList(new Object[][] {
			// x, y, obstacles
			{ 5, 5, new int[][] { {0,0} } },
			{ 0, 0, new int[][] { {0,0} } },
			{ -5, -5, new int[][] { {0,0} } }
		});
	};
	
	private final int x;
	private final int y;
	private final int[][] obstacles;
	// Test object
	private Planet planet;
	
	public PlanetTest(int x, int y, int[][] obstacles) {
		this.x = x;
		this.y = y;
		this.obstacles = obstacles;
	}
	
	@Before
	public void setUp() throws Exception {
		planet = new Planet(x, y);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testSetGetTerrain() {
		for (int[] point : obstacles) {
			planet.setTerrain(point[0], point[1], Terrain.OBSTACLE);
			Terrain terrain = planet.getTerrainAt(point[0], point[1]);
			
			assertEquals(Terrain.OBSTACLE, terrain);
		}
	}

	@Test
	public void testGetSizeX() {
		assertEquals(x, planet.getSizeX());
	}

	@Test
	public void testGetSizeY() {
		assertEquals(y, planet.getSizeY());
	}

}
