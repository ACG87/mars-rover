package acarriong.domain;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.assertj.core.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
@RunWith(Parameterized.class)
public class CoordinateTest {
	
	@Parameters
	public static List<Object> data() {
		return Arrays.asList(new Object[][] {
			// x, y, obstacles
			{ 5, 5},
			{ 0, 0},
			{ -5, -5}
		});
	};
	private int x;
	private int y;
	
	private Coordinate coordinate;
	
	public CoordinateTest(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Before
	public void setUp() throws Exception {
		coordinate = new Coordinate(x, y);
	}

	

	@Test
	public void testNextCoordinate() {
		
	}

	@Test
	public void testGetX() {
		assertEquals(x, coordinate.getX());
	}


	@Test
	public void testGetY() {
		assertEquals(y, coordinate.getY());
	}


}
