package acarriong.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;

import org.assertj.core.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import acarriong.domain.enums.MarsRoverEnums.Direction;
@RunWith(Parameterized.class)
public class RoverTest {
	
	@Parameters
	public static List<Object> data() {
		return Arrays.asList(new Object[][] {
			// position, direction, planet
			{ new Coordinate(5,6),Direction.findByCode("N"), new Planet(7, 8) },
			{ new Coordinate(0,-1),Direction.findByCode("W"), new Planet(-1, -5) },
			{ new Coordinate(0,0),Direction.findByCode("S"), new Planet(7, 8) },
			{ new Coordinate(5,6),Direction.findByCode("E"), new Planet(7, 8) }
		});
		
	};

	private Coordinate position;
	private Direction direction;
	private Planet planet;
	
	private Rover rover;
	
	public RoverTest(Coordinate position, Direction direction, Planet planet) {
		this.position = position;
		this.direction = direction;
		this.planet = planet;
	}
	
	@Before
	public void setUp() throws Exception {
		rover = new Rover(position,direction,planet);
	}

	@Test
	public void testRover() {
	}

	@Test
	public void testDoCommand() {
	}

	@Test
	public void testGetPosition() {
		assertEquals(position, rover.getPosition());
	}

	

	@Test
	public void testGetDirection() {
		assertEquals(direction, rover.getDirection());
	}

	

}
