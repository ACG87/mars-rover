package acarriong.controller.error;

import java.util.Date;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonFormat;

public class MarsRoverError {

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
	private final Date timestamp;
	private final HttpStatus status;
	private final String message;
	
	public MarsRoverError(HttpStatus status, String message) {
		this.timestamp = new Date();
		this.status = status;
		this.message = message;
	}

	public Date getTimestamp() {
		return timestamp;
	}
	
	public HttpStatus getStatus() {
		return status;
	}
	
	public String getMessage() {
		return message;
	}
	
	
}
