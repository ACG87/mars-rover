package acarriong.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import acarriong.domain.Coordinate;
import acarriong.domain.Planet;
import acarriong.domain.Rover;
import acarriong.domain.enums.MarsRoverEnums.Command;
import acarriong.domain.enums.MarsRoverEnums.Direction;
import acarriong.dto.CommandResponseDTO;
import acarriong.dto.RoverPositionDTO;
import acarriong.service.PlanetService;
import acarriong.service.RoverService;

@RestController
@RequestMapping("/rover/*")
public class RoverController {

	@Autowired
	private RoverService roverService;
	
	@Autowired
	private PlanetService planetService;
	
	@RequestMapping(path = "/createRover",method = RequestMethod.POST, consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
	public RoverPositionDTO createRover(@RequestBody(required = true) RoverPositionDTO initialPosition)  {
		Coordinate position = new Coordinate(initialPosition.getX(), initialPosition.getY());
		
		
		if(position == null) {
			throw new NullPointerException("Unknown position in input");
		}
		
		Direction direction = Direction.findByCode(initialPosition.getDirectionCode());
		
		// CALL SERVICE
		Planet planet = planetService.getPlanet();
		if(planet == null) {
			throw new NullPointerException("Planet is null");
		}
		planet.getSizeX();
		planet.getSizeY();
		RoverPositionDTO roverStatus;
		roverStatus = new RoverPositionDTO();
		roverStatus.setRoverCreated(roverService.createRover(position, direction, planet));
		Rover rover = roverService.getRover();
		if(rover != null) {
			roverStatus.setDirectionCode(rover.getDirection().getCode());
			roverStatus.setX(rover.getPosition().getX());
			roverStatus.setY(rover.getPosition().getY());
		}
		return roverStatus;
	}
	  
	@RequestMapping(path = "/executeCommand", method = RequestMethod.PUT, consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
	public CommandResponseDTO executeCommand(@RequestBody(required = true) String commandWord) throws Exception {
		List<Command> commandList = new ArrayList<>();
		for (char chr : commandWord.toCharArray()) {
			Command comm = Command.findByCode(chr);
			
			if (comm == null) {
				throw new NullPointerException("Unknown command in input");
			}
			
			commandList.add(comm);
		}
		
		boolean allExecuted = roverService.executeCommands(commandList);
		
		
		CommandResponseDTO response = new CommandResponseDTO();
		Rover rover = roverService.getRover();
		RoverPositionDTO roverStatus = new RoverPositionDTO();
		roverStatus.setDirectionCode(rover.getDirection().getCode());
		roverStatus.setX(rover.getPosition().getX());
		roverStatus.setY(rover.getPosition().getY());
		roverStatus.setCollisionObstacle(allExecuted);
		response.setRoverStatus(roverStatus); 
		response.setObstacle(!allExecuted);
		
		return response;
	}
}
