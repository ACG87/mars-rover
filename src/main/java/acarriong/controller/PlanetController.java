package acarriong.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import acarriong.domain.Coordinate;
import acarriong.domain.Planet;
import acarriong.dto.PlanetResponseDTO;
import acarriong.dto.PointDTO;
import acarriong.dto.RoverPositionDTO;
import acarriong.service.PlanetService;

@RestController
@RequestMapping("/planet/*")
public class PlanetController {

	@Autowired
	private PlanetService planetService;
	
	@RequestMapping(path = "/createPlanet", method = RequestMethod.POST, consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
	public PlanetResponseDTO createMap(@RequestBody(required = true) PointDTO point) throws Exception {
		if (point == null) {
			throw new NullPointerException("Input point can't be null");
		}
		Planet planet = planetService.createPlanet(point.getX(), point.getY());
		
		return new PlanetResponseDTO(planet);
	}
	
	@RequestMapping(path = "/createObstacle", method = RequestMethod.POST, consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
	public PlanetResponseDTO setObstacle(@RequestBody(required = true) PointDTO point) throws Exception {
		if (point == null) {
			throw new NullPointerException("Input point can't be null");
		}
		Planet planet = planetService.createObstacle(point.getX(), point.getY());
		PlanetResponseDTO planetResponseDTO;
		planetResponseDTO = new PlanetResponseDTO(planet);
		
		planetResponseDTO.setCreated(planet.isCreateObstacle());
		
		
		return planetResponseDTO;
	}
	
	@RequestMapping(path = "/createObstacles", method = RequestMethod.POST, consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
	public PlanetResponseDTO setObstacles(@RequestBody(required = true) List<PointDTO> listCoor) throws Exception {
		if (listCoor == null) {
			throw new NullPointerException("Input point can't be null");
		}
		Planet planet = planetService.createObstacles(listCoor);

		PlanetResponseDTO planetResponseDTO;
		planetResponseDTO = new PlanetResponseDTO(planet);
		
		planetResponseDTO.setCreated(planet.isCreateObstacle());
		
		
		return planetResponseDTO;
	}
	
}
