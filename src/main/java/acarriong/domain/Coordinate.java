package acarriong.domain;

import acarriong.domain.enums.MarsRoverEnums.Direction;

public class Coordinate {

	private int x;
	private int y;
	
	public Coordinate(int x, int y) {
		if (x <= 0 || y <= 0) {
			throw new IndexOutOfBoundsException();
		}
		this.x = x;
		this.y = y;
	}

	public Coordinate nextCoordinate(Direction direction) {
		Coordinate target;
		
		switch (direction) {
		case EAST:
			target = new Coordinate(getX() + 1, getY());
			break;
		case NORTH:	
			target = new Coordinate(getX(), getY() + 1);
			break;
		case SOUTH:
			target = new Coordinate(getX(), getY() - 1);
			break;
		case WEST:
			target = new Coordinate(getX() - 1, getY());
			break;
		default:
			throw new IllegalArgumentException("Unknown direction");
			
		}
		
		return target;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	
}
