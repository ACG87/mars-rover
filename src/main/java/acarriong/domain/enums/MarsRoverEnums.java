package acarriong.domain.enums;

public class MarsRoverEnums {

	/**
	 * 
	 * @author acarriong
	 *
	 */
	public enum Terrain {
		LAND(0),
		OBSTACLE(1);
		
		private final int code;
		
		private Terrain(int code) {
			this.code = code;
		}
		
		public int getCode() {
			return code;
		}
	}
	
	public enum Direction {
		EAST("E"),
		NORTH("N"),
		SOUTH("S"),
		WEST("W");
		
		private final String code;
		
		private Direction(String code) {
			this.code = code;
		}
		
		public String getCode() {
			return code;
		}
		
		public static Direction findByCode(String code) {
			String trimCode = code.trim().toUpperCase();
			for (Direction dir : values()) {
				if (dir.getCode().equals(trimCode)) {
					return dir;
				}
			}
			
			return null;
		}
	}
	
	public enum Command {
		BACKWARD('b'),
		FORWARD('f'),
		TURN_LEFT('l'),
		TURN_RIGHT('r');

		private final char code;
		
		private Command(char code) {
			this.code = code;
		}
		
		public char getCode() {
			return code;
		}
		
		public static Command findByCode(char code) {
			for (Command comm : values()) {
				if (comm.getCode() == code) {
					return comm;
				}
			}
			
			return null;
		}
	}
}
