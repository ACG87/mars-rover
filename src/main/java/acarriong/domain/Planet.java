package acarriong.domain;

import java.io.Serializable;

import acarriong.domain.enums.MarsRoverEnums.Terrain;

public class Planet implements Serializable {

	private static final long serialVersionUID = 7987246163433942463L;
	
	// Suposicion: Todos los mapas empiezan en (0,0) esquina abajo-izquierda
	/**
	 * (0,sizeY) . . . (sizeX,sizeY)
	 * .				.
	 * .				.
	 * .				.
	 * (0,0) . . . . . (sizeX,0)
	 * 
	 * (i,j)
	 */
	private Terrain[][] grid;
	private boolean createObstacle;
	

	public Planet(int sizeX, int sizeY) {
		if (sizeX <= 0 || sizeY <= 0) {
			throw new IndexOutOfBoundsException();
		}
		grid = new Terrain[sizeX][sizeY];
		fillMap(Terrain.LAND);
	}
	
	// PUBLIC
	public void setTerrain(int x, int y, Terrain terrain) {
		if (grid == null || terrain == null) {
			// TODO Exception
		}
		
		if (x < 0 || x > grid.length) {
			// TODO Exception (Out of bounds)
		}

		if (y < 0 || y > grid[0].length) {
			// TODO Exception (Out of bounds)
		}
		
		grid[x][y] = terrain;
	}
	
	public Terrain getTerrainAt(int x, int y) {
		return grid[x][y];
	}
	
	public int getSizeX() {
		return grid.length;
	}
	
	public int getSizeY() {
		return grid[0].length;
	}
	
	// PRIVATE
	private void fillMap(Terrain terrain) {
		// TODO COmprobar not null, grid inicial
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[i].length; j++) {
				grid[i][j] = terrain;
			}
		}
	}

	public boolean isCreateObstacle() {
		return createObstacle;
	}

	public void setCreateObstacle(boolean createObstacle) {
		this.createObstacle = createObstacle;
	}
}
