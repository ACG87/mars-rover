package acarriong.domain;

import acarriong.domain.enums.MarsRoverEnums.Command;
import acarriong.domain.enums.MarsRoverEnums.Direction;
import acarriong.domain.enums.MarsRoverEnums.Terrain;

public class Rover {

	private Coordinate position;
	private Direction direction;
	private Planet planet;
	
	public Rover(Coordinate position, Direction direction, Planet planet) {
		if (position == null) {
			throw new NullPointerException("position is null");
		}
		if (direction == null) {
			throw new NullPointerException("direction is null");
		}
		if (planet == null) {
			throw new NullPointerException("Planet is null");
		}
		this.position = position;
		this.direction = direction;
		this.planet = planet;
	}
	
	/**
	 * 
	 * @param comm
	 * @return true if the command has been executed. Otherwise, returns false;
	 */
	public boolean doCommand(Command command) {
		if (command == null) {
			return false;
		}
		
		boolean executed = false;
		switch(command) {
		case BACKWARD:
			executed = move(true);
			break;
		case FORWARD:
			executed = move(false);
			break;
		case TURN_LEFT:
			turnLeft();
			executed = true;
			break;
		case TURN_RIGHT:
			turnRight();
			executed = true;
			break;
		default:
			throw new IllegalArgumentException("Unknown command");
		}
		
		return executed;
	}
	
	private boolean move(boolean backward) {
		Coordinate target = getNextPositionInPlanet(backward);
		
		Terrain terrainInTarget = planet.getTerrainAt(target.getX(), target.getY());
		
		if (Terrain.OBSTACLE.equals(terrainInTarget)) {
			return false; // Si hay obstaculo no se mueve e informa
		}
		else {
			position = target;
		}
		
		return true;
	}
	
	private void turnLeft() {
		switch(direction) {
		case EAST:
			direction = Direction.NORTH;
			break;
		case NORTH:
			direction = Direction.WEST;
			break;
		case SOUTH:
			direction = Direction.EAST;
			break;
		case WEST:
			direction = Direction.SOUTH;
			break;
		default:
			throw new IllegalArgumentException("Unknown direction");
		}
	}
	
	private void turnRight() {
		switch(direction) {
		case EAST:
			direction = Direction.SOUTH;
			break;
		case NORTH:
			direction = Direction.EAST;
			break;
		case SOUTH:
			direction = Direction.WEST;
			break;
		case WEST:
			direction = Direction.NORTH;
			break;
		default:
			throw new IllegalArgumentException("Unknown direction");
		}
		
	}

	private Coordinate getNextPositionInPlanet(boolean backward) {
		Coordinate next;
		if (backward) {
			next = position.nextCoordinate(reverseDirection(getDirection()));
		}
		else {
			next = position.nextCoordinate(getDirection());
		}
		
		if (next.getX() < 0) {
			next.setX(planet.getSizeX() - 1);
		}
		else if (next.getX() >= planet.getSizeX()) {
			next.setX(0);
		}
		
		if (next.getY() < 0) {
			next.setY(planet.getSizeY() - 1);
		}
		else if (next.getY() >= planet.getSizeY()) {
			next.setY(0);
		}
		
		return next;
	}
	
	private Direction reverseDirection(final Direction direction) {
		switch(direction) {
		case EAST:
			return Direction.WEST;
		case NORTH:
			return Direction.SOUTH;
		case SOUTH:
			return Direction.NORTH;
		case WEST:
			return Direction.EAST;
		default:
			throw new IllegalArgumentException("Unknown direction");
		}
	}
	
	public Coordinate getPosition() {
		return position;
	}

	public void setPosition(Coordinate position) {
		this.position = position;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}	
}
