package acarriong.dto;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;

import acarriong.domain.Planet;

public class PlanetResponseDTO implements Serializable {

	private static final long serialVersionUID = 2317253631904026792L;

	private int[][] grid;
	
	private String created;
	
	
	
	public PlanetResponseDTO() {}

	public PlanetResponseDTO(Planet planet) {
		int x = planet.getSizeX();
		int y = planet.getSizeY();
		
		grid = new int[x][y];
		
		for (int i = 0; i < x; i++) {
			for (int j = 0; j < y; j++) {
				grid[i][j] = planet.getTerrainAt(i, j).getCode();
			}
		}
	}
	
	public int[][] getGrid() {
		return grid;
	}

	public void setGrid(int[][] grid) {
		this.grid = grid;
	}
	
	public String getCreated() {
		return created;
	}
	public void setCreated(boolean isCreated) {
		if(isCreated) {			
			this.created = "Elements created";
		}else {
			this.created = "Has Elements not created";
		}
	}
	
	
}
