package acarriong.dto;

import java.util.LinkedHashMap;
import java.util.List;

public class RoverPositionDTO {

	private int x;
	private int y;
	private String directionCode;
	private String roverCreated;
	private String collision;
	
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public String getDirectionCode() {
		return directionCode;
	}
	public void setDirectionCode(String directionCode) {
		this.directionCode = directionCode;
	}

	public String getRoverCreated() {
		return roverCreated;
	}
	
	public void setRoverCreated(boolean isCreated) {
		if(isCreated) {
			roverCreated = "Rover created";
		}else {
			roverCreated = "Rover not created, has obstacle in this coordinate";
		}
	}
	public String getCollisionObstacle() {
		return collision;
	}
	public void setCollisionObstacle(boolean isCreated) {
		if(isCreated) {			
			this.collision = "Rover no collision";
		}else {
			this.collision = "Rover collision";
		}
	}
	
	public void setRoverCreated(String roverCreated) {
		this.roverCreated = roverCreated;
	}
	
	
}
