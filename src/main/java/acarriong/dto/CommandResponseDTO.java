package acarriong.dto;

public class CommandResponseDTO {

	private RoverPositionDTO roverStatus;
	private boolean obstacle;
	public RoverPositionDTO getRoverStatus() {
		return roverStatus;
	}
	public void setRoverStatus(RoverPositionDTO roverStatus) {
		this.roverStatus = roverStatus;
	}
	public boolean isObstacle() {
		return obstacle;
	}
	public void setObstacle(boolean obstacle) {
		this.obstacle = obstacle;
	}
	
	
}
