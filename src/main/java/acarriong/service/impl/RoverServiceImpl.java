package acarriong.service.impl;

import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Service;

import acarriong.domain.Coordinate;
import acarriong.domain.Planet;
import acarriong.domain.Rover;
import acarriong.domain.enums.MarsRoverEnums.Command;
import acarriong.domain.enums.MarsRoverEnums.Direction;
import acarriong.domain.enums.MarsRoverEnums.Terrain;
import acarriong.service.RoverService;

@Service
public class RoverServiceImpl implements RoverService {

	private Rover rover;

	@Override
	public boolean createRover(Coordinate position, Direction direction, Planet planet) {
		if(position.getX() > planet.getSizeX()){
			throw new IndexOutOfBoundsException("Out of Planet");
		}
		if(position.getY() > planet.getSizeY()) {
			throw new IndexOutOfBoundsException("Out of Planet");
		}
		Terrain terrainInTarget = planet.getTerrainAt(position.getX(), position.getY());
		
		if (Terrain.OBSTACLE.equals(terrainInTarget)) {
			return false; 
		}else {
			rover = new Rover(position,direction,planet);// TODO Auto-generated method stub
			return true;
		}	
		
	}
	
	@Override
	public boolean executeCommands(List<Command> commandList) {
		if (rover == null) {
			throw new NullPointerException("Any rover available");
		}
		
		boolean executed = true;
		Iterator<Command> iter = commandList.iterator();
		while (executed && iter.hasNext()) {
			Command command = iter.next();
			executed = rover.doCommand(command); // Retorna falso solo si trata de moverse y hay obstaculo
		}
		
		return executed;
	}

	@Override
	public Rover getRover() {
		return rover;
	}

	
}
