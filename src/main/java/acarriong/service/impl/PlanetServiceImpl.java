package acarriong.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import acarriong.domain.Coordinate;
import acarriong.domain.Planet;
import acarriong.domain.enums.MarsRoverEnums.Terrain;
import acarriong.dto.PointDTO;
import acarriong.service.PlanetService;

@Service
public class PlanetServiceImpl implements PlanetService {

	private Planet currentPlanet;
	
	/**
	 * @throws Exception 
	 * @see acarriong.service.PlanetService#createPlanet(int, int)
	 */
	@Override
	public Planet createPlanet(int x, int y) throws Exception {
		if((x == 0 && y == 0) || (x < 0 || y < 0)) {
			throw new Exception("Can't create a Planet with this coordinate");			
		}else {
			currentPlanet = new Planet(x, y);
		}
		return currentPlanet;
	}

	/**
	 * @throws Exception 
	 * @see acarriong.service.PlanetService#createObstacle(int, int)
	 */
	@Override
	public Planet createObstacle(int x, int y) throws Exception {
		if (currentPlanet != null) {
			Terrain terrain = currentPlanet.getTerrainAt(x, y);
			if(!Terrain.OBSTACLE.equals(terrain)) {
				currentPlanet.setTerrain(x,y , Terrain.OBSTACLE);
				currentPlanet.setCreateObstacle(true);
			}else {
				currentPlanet.setCreateObstacle(false);
			}
		}else {
			throw new NullPointerException("Planet is null");
		}
		return currentPlanet;
	}

	@Override
	public Planet getPlanet() {
		return currentPlanet;
	}

	@Override
	public Planet createObstacles(List<PointDTO> listCoor) throws Exception {
		if (currentPlanet != null) {
			currentPlanet.setCreateObstacle(true);
			for(PointDTO coor: listCoor ){
				Coordinate coordinate = new Coordinate(coor.getX(), coor.getY());
				Terrain terrainInTarget = currentPlanet.getTerrainAt(coordinate.getX(), coordinate.getY());
				if(!Terrain.OBSTACLE.equals(terrainInTarget)) {
					currentPlanet.setTerrain(coordinate.getX(), coordinate.getY(), Terrain.OBSTACLE);
				}else {
					currentPlanet.setCreateObstacle(false);
				}
			}		
		}else {
			throw new NullPointerException("Planet is null");
		}
		return currentPlanet;
	}
	

}
