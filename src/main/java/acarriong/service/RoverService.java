package acarriong.service;

import java.util.List;

import acarriong.domain.Coordinate;
import acarriong.domain.Planet;
import acarriong.domain.Rover;
import acarriong.domain.enums.MarsRoverEnums.Command;
import acarriong.domain.enums.MarsRoverEnums.Direction;

public interface RoverService {

	/**
	 * 
	 * @param commandList
	 * @return true if all the commands have been executed. Otherwise, returns false (obstacle)
	 */
	public boolean executeCommands(List<Command> commandList);
	
	public boolean createRover(Coordinate position, Direction direction, Planet planet);
	
	public Rover getRover();
}
