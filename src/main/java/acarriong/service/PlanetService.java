package acarriong.service;

import java.util.List;

import acarriong.domain.Planet;
import acarriong.dto.PointDTO;

public interface PlanetService {

	/**
	 * Creates a new planet witch size (x,y).
	 * This planet doesn't have any obstacle
	 * @param x
	 * @param y
	 * @return
	 */
	public Planet createPlanet(int x, int y)  throws Exception;
	
	/**
	 * Create an obstacle in the given point
	 * @param x
	 * @param y
	 * @return
	 */
	public Planet createObstacle(int x, int y) throws Exception;
	
	public Planet createObstacles(List<PointDTO> listCoor) throws Exception;
	
	public Planet getPlanet();
}
