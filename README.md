##

Para realizar las pruebas recomiendo descargar POSTMAN, tiene una interfaz intuitiva y f�cil de usar.
Copio el link de decarga:
https://www.getpostman.com/apps

Aqui dejo el listado de comandos para poder realizar las pruebas:

----Crear Planeta----

POST (http://localhost:8080/planet/createPlanet) (JSON (application/json)

Ejemplo Body request:
{"x":20,"y":20}

----Crear Obstaculo----

POST (http://localhost:8080/planet/createObstacle) (JSON (application/json)

Ejemplo Body request:
{"x":7,"y":6}

----Crear Obstaculos----

POST (http://localhost:8080/planet/createObstacles) (JSON (application/json)

Ejemplo Body request:
[{"x":4,"y":4},
{"x":6,"y":6},
{"x":7,"y":4},
{"x":8,"y":4},
{"x":4,"y":9},
{"x":4,"y":10}]

----Crear Rover----

POST (http://localhost:8080/rover/createRover) (JSON (application/json)

Ejemplo Body request:
{"x":6,"y":8,"directionCode":"E"}


----Mover Rover----

PUT (http://localhost:8080/rover/executeCommand) (JSON (application/json)

Ejemplo Body request:
ffrbll

